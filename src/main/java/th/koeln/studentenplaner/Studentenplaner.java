package th.koeln.studentenplaner;

import th.koeln.studentenplaner.database.DatabaseType;
import th.koeln.studentenplaner.database.types.MySQLType;
import th.koeln.studentenplaner.database.types.OracleType;

import java.util.Scanner;

public class Studentenplaner {
    private DatabaseType databaseType;

    public static void main(String[] args) {
        Studentenplaner studentenplaner = new Studentenplaner();
        studentenplaner.run();
    }

    private void run() {
        System.out.print("Choose one database system (MySQL, Oracle): ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.next();
        databaseType = getDatabaseType(input);
        if(databaseType == null)
            System.exit(-1);
        while (true) {
            System.out.println("What do you want to do?");
            System.out.println("1) Register");
            System.out.println("2) Enroll");
            System.out.println("3) De-Register");
            System.out.println("etc.");
            scanner = new Scanner(System.in);
            input = scanner.next();
            switch (input.toLowerCase()) {
                case "1":
                    break;
            }
        }
    }

    private DatabaseType getDatabaseType(String input) {
        switch (input.toLowerCase()) {
            case "mysql":
            case "m":
                // th_koeln 0000
                String[] credentials = this.getCredentials();
                return new MySQLType("hardenacke-oe.de", 3306, "th_koeln_studentenplaner", credentials[0], credentials[1]);
            case "oracle":
            case "o":
                // gmId password (VPN!)
                credentials = this.getCredentials();
                return new OracleType("studidb.gm.th-koeln.de", 1521, "vlesung", credentials[0], credentials[1]);
            default:
                System.out.println("Type '" + input + "' not found!");
                return null;
        }
    }

    private String[] getCredentials() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please insert your username: ");
        String userName = scanner.next();
        System.out.print("Please insert your password: ");
        String password = scanner.next();
        return new String[]{userName, password};
    }

    public DatabaseType getDatabaseType() {
        return databaseType;
    }
}
