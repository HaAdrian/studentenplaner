package th.koeln.studentenplaner.database;

import java.sql.Connection;

public interface DatabaseType {

    boolean setupDatabase();

    Connection createConnection();

    boolean releaseConnection(Connection connection);

    boolean testConnection(Connection connection);

    void registerStudent();

    void enrollStudent();

    void deRegisterStudent();
}
