package th.koeln.studentenplaner.database.types;

import th.koeln.studentenplaner.database.DatabaseType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLType implements DatabaseType {
    private final String host;
    private final int port;
    private final String database;
    private final String userName;
    private final String password;

    public MySQLType(String host, int port, String database, String userName, String password) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.userName = userName;
        this.password = password;
        if (!testConnection(this.createConnection())) {
            System.out.println("Cannot create database connection!");
            System.exit(-1);
        }
        setupDatabase();
    }

    @Override
    public boolean setupDatabase() {
        Connection connection = this.createConnection();
        try {
            Statement state = connection.createStatement();
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Semester(Semesternummer INT PRIMARY KEY, Semestergebuehren int);");
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Modul (Modulnummer INT PRIMARY KEY, Creditpoints INT);");
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Preufung (preufungsnummer Int PRIMARY key, modulnummer INT);");
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Veranstaltung (veranstaltungsnummer INT PRIMARY KEY , zeitpunkt DATE NOT NULL, Note INT);");
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Student (matrikelnummer varchar(55) PRIMARY KEY , semesternummer INT NOT NULL, vorname VARCHAR(55) NOT NULL, name VARCHAR(55) NOT NULL, adresse VARCHAR(55) NOT NULL, geburtsdatum DATE NOT NULL, hoererstatus varchar(25), Urlaubssemester INT, Abschlussziel varchar(25));");
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Preufungsanmeldung (preufungsnummer INT NOT NULL PRIMARY KEY, studentMatrikelnummer varchar(55) NOT NULL);");
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Praktikumanmeldung (studentMatrikelnummer varchar(55) NOT NULL PRIMARY KEY, modulveranstaltung INT NOT NULL, modulveranstaltungsnummer INT NOT NULL);");
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Modulveranstaltung (modulnummer INT NOT NULL PRIMARY KEY, veranstaltungsnummer INT NOT NULL);");
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Modulteilnehmer (modulnummer INT NOT NULL, studentMatrikelnummer varchar(55) NOT NULL, note INT  NOT NULL);");
            state.executeUpdate("CREATE TABLE IF NOT EXISTS Semester_Student (Semesternummer INT NOT NULL, Matrikelnummer varchar(55) NOT NULL, bezahlte_gebuehren DOUBLE NOT NULL);");

            /*state.executeUpdate("ALTER TABLE Semester_Student Add CONSTRAINT student_semester_fk FOREIGN Key (Semesternummer) REFERENCES Semester (Semesternummer);");
            state.executeUpdate("ALTER TABLE Semester_Student Add CONSTRAINT student_semester_fk2 FOREIGN Key (Matrikelnummer) REFERENCES Student (matrikelnummer);");
            state.executeUpdate("ALTER TABLE Preufung ADD CONSTRAINT modulnummer_fk FOREIGN KEY (Modulnummer) REFERENCES Modul(modulnummer);");
            state.executeUpdate("ALTER TABLE Student Add CONSTRAINT semesternummer_fk FOREIGN Key (semesternummer) REFERENCES Semester (semesternummer);");
            state.executeUpdate("ALTER TABLE Preufungsanmeldung  Add CONSTRAINT preufungsnummer_fk FOREIGN Key (preufungsnummer) REFERENCES Preufung (preufungsnummer);");
            state.executeUpdate("ALTER TABLE Preufungsanmeldung Add CONSTRAINT studentMatrikelnummer_fk FOREIGN Key (studentMatrikelnummer) REFERENCES Student (matrikelnummer);");
            state.executeUpdate("ALTER TABLE Praktikumanmeldung Add CONSTRAINT studentMatrikelnummer_fk1 FOREIGN Key (studentMatrikelnummer) REFERENCES Student (matrikelnummer);");
            state.executeUpdate("ALTER TABLE Praktikumanmeldung Add CONSTRAINT modulveranstaltung_fk FOREIGN Key (modulveranstaltung) REFERENCES Modul (modulnummer);");
            state.executeUpdate("ALTER TABLE Praktikumanmeldung Add CONSTRAINT modulveranstaltungsnummer_fk FOREIGN Key (modulveranstaltungsnummer) REFERENCES Veranstaltung (veranstaltungsnummer);");
            state.executeUpdate("ALTER TABLE Modulveranstaltung Add CONSTRAINT modulnummer_fk1 FOREIGN Key (modulnummer) REFERENCES Modul (modulnummer);");
            state.executeUpdate("ALTER TABLE Modulveranstaltung Add  CONSTRAINT veranstaltungsnummer_fk FOREIGN Key (veranstaltungsnummer) REFERENCES Veranstaltung (veranstaltungsnummer);");
            state.executeUpdate("ALTER TABLE Modulteilnehmer Add  CONSTRAINT modulnummer_fk2 FOREIGN Key (modulnummer) REFERENCES Modul (modulnummer);");
            state.executeUpdate("ALTER TABLE Modulteilnehmer Add CONSTRAINT studentMatrikelnummer_fk2 FOREIGN Key (studentMatrikelnummer) REFERENCES Student (matrikelnummer);");
            */
            state.close();
            return this.releaseConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Connection createConnection() {
        Connection connection;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://" + this.host + ":" + this.port + "/" + database + "?autoReconnect=true&useSSL=false",
                    this.userName, this.password);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return connection;
    }

    @Override
    public boolean releaseConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean testConnection(Connection connection) {
        return releaseConnection(connection);
    }

    @Override
    public void registerStudent() {
        Connection connection = this.createConnection();
        try {
            Statement state = connection.createStatement();

            state.executeUpdate("INSERT INTO `Students` VALUES(0)");

            state.close();
            this.releaseConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void enrollStudent() {

    }

    @Override
    public void deRegisterStudent() {

    }
}
