package th.koeln.studentenplaner.database.types;

import th.koeln.studentenplaner.database.DatabaseType;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class OracleType implements DatabaseType {

    private final String host;
    private final int port;
    private final String database;
    private final String userName;
    private final String password;

    public OracleType(String host, int port, String database, String userName, String password) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.userName = userName;
        this.password = password;
        if (!testConnection(this.createConnection())) {
            System.out.println("Cannot create database connection!");
            System.exit(-1);
        }
    }

    @Override
    public boolean setupDatabase() {
        Connection connection = this.createConnection();
        try {
            Statement state = connection.createStatement();
            state.executeUpdate("Drop Table Modul CASCADE CONSTRAINTS;");
            state.executeUpdate("Drop Table Prüfung CASCADE CONSTRAINTS;");
            state.executeUpdate("Drop Table Veranstaltung CASCADE CONSTRAINTS;");
            state.executeUpdate("Drop Table Semester CASCADE CONSTRAINTS;");
            state.executeUpdate("Drop Table Student CASCADE CONSTRAINTS;");
            state.executeUpdate("Drop Table Prüfungsanmeldung CASCADE CONSTRAINTS;");
            state.executeUpdate("Drop Table Praktikumsanmeldung CASCADE CONSTRAINTS;");
            state.executeUpdate("Drop Table Modulveranstaltung CASCADE CONSTRAINTS;");
            state.executeUpdate("Drop Table Modulteilnehmer CASCADE CONSTRAINTS;");

            state.executeUpdate("Create Table Modul (\n" +
                    "modulnummer Int PRIMARY KEY,\n" +
                    "creditpoints Int Null\n" +
                    ") ;");
            state.executeUpdate("Create TABLE Prüfung (\n" +
                    "prüfungsnummer Int PRIMARY key ,\n" +
                    "modulnummer Int NOT Null ,\n" +
                    "\n" +
                    "CONSTRAINT modulnummer_fk FOREIGN KEY (modulnummer) REFERENCES Modul (modulnummer)\n" +
                    ");");
            state.executeUpdate("Create Table Veranstaltung (\n" +
                    "veranstaltungsnummer Int PRIMARY KEY ,\n" +
                    "zeitpunkt date NOT NULL\n" +
                    ");");
            state.executeUpdate("Create  Table Semester (\n" +
                    "semesternummer INT PRIMARY KEY\n" +
                    ");");
            state.executeUpdate("Create Table Student (\n" +
                    "matrikelnummer Int PRIMARY KEY ,\n" +
                    "semesternummer Int NOT NULL,\n" +
                    "vorname VARCHAR(255) NOT NULL,\n" +
                    "name VARCHAR(255) NOT NULL,\n" +
                    "adresse VARCHAR(255) NOT NULL,\n" +
                    "geburtsdatum DATE NOT NULL,\n" +
                    "\n" +
                    "CONSTRAINT semesternummer_fk FOREIGN Key (semesternummer) REFERENCES Semester (semesternummer)\n" +
                    ");");
            state.executeUpdate("Create Table Prüfungsanmeldung (\n" +
                    "prüfungsnummer INT NOT NULL,\n" +
                    "studentMatrikelnummer INT NOT NULL,\n" +
                    "\n" +
                    "CONSTRAINT prüfungsnummer_fk FOREIGN Key (prüfungsnummer) REFERENCES Prüfung (prüfungsnummer),\n" +
                    "CONSTRAINT studentMatrikelnummer_fk FOREIGN Key (studentMatrikelnummer) REFERENCES Student (matrikelnummer)\n" +
                    ");");
            state.executeUpdate("Create Table Praktikumanmeldung (\n" +
                    "studentMatrikelnummer INT NOT NULL ,\n" +
                    "modulveranstaltung INT NOT NULL,\n" +
                    "modulveranstaltungsnummer INT NOT NULL,\n" +
                    "\n" +
                    "CONSTRAINT studentMatrikelnummer_fk1 FOREIGN Key (studentMatrikelnummer) REFERENCES Student (matrikelnummer),\n" +
                    "CONSTRAINT modulveranstaltung_fk FOREIGN Key (modulveranstaltung) REFERENCES Modul (modulnummer),\n" +
                    "CONSTRAINT modulveranstaltungsnummer_fk FOREIGN Key (modulveranstaltungsnummer) REFERENCES Veranstaltung (veranstaltungsnummer)\n" +
                    ");");
            state.executeUpdate("Create Table Modulveranstaltung (\n" +
                    "modulnummer INT NOT NULL,\n" +
                    "veranstaltungsnummer INT NOT NULL ,\n" +
                    "\n" +
                    "CONSTRAINT modulnummer_fk1 FOREIGN Key (modulnummer) REFERENCES Modul (modulnummer),\n" +
                    "CONSTRAINT veranstaltungsnummer_fk FOREIGN Key (veranstaltungsnummer) REFERENCES Veranstaltung (veranstaltungsnummer)\n" +
                    ");");
            state.executeUpdate("Create Table Modulteilnehmer (\n" +
                    "modulnummer INT NOT NULL ,\n" +
                    "studentMatrikelnummer INT NOT NULL,\n" +
                    "note INT  NOT NULL,\n" +
                    "\n" +
                    "CONSTRAINT modulnummer_fk2 FOREIGN Key (modulnummer) REFERENCES Modul (modulnummer),\n" +
                    "CONSTRAINT studentMatrikelnummer_fk2 FOREIGN Key (studentMatrikelnummer) REFERENCES Student (matrikelnummer)\n" +
                    ");");
            state.close();
            return this.releaseConnection(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Connection createConnection() {
        Connection connection;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(
                    "jdbc:oracle:thin:@" + this.host + ":" + this.port + ":" + this.database, this.userName, this.password);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return connection;
    }

    @Override
    public boolean releaseConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean testConnection(Connection connection) {
        return releaseConnection(connection);
    }

    @Override
    public void registerStudent() {

    }

    @Override
    public void enrollStudent() {

    }

    @Override
    public void deRegisterStudent() {

    }
}
